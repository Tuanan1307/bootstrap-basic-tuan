import React from "react";
import { Container, Row } from "react-bootstrap";
const OurTeam = () => {
  return (
    <div className="ourteam">
      <Container className=" p-5">
        <Row>
          <div
            className="top h1 d-flex justify-content-center py-5"
            style={{ color: "#F97300" }}
          >
            Our Team
          </div>
          <div className="bottom">
            <div className="col-12 d-flex">
              <div className="col-3">
                <img src={require("../accset/team-3.jpg")} />
                <div>John</div>
                <span className="text-black-50">Manager</span>
              </div>
              <div className="col-3">
                <img src={require("../accset/team-2.jpg")} />
                <div>Sara</div>
                <span className="text-black-50">S.Enginer</span>
              </div>
              <div className="col-3">
                <img src={require("../accset/team-3.jpg")} />
                <div>John</div>
                <span className="text-black-50">Front End Deverloper</span>
              </div>
              <div className="col-3">
                <img src={require("../accset/team-2.jpg")} />
                <div>Sara</div>
                <span className="text-black-50">Team Manger</span>
              </div>
            </div>
          </div>
        </Row>
      </Container>
    </div>
  );
};

export default OurTeam;
