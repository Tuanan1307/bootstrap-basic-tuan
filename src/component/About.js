import React from "react";
import "./About.scss";
import { Container, Row } from "react-bootstrap";
import img from "../accset/team-3.jpg";
const About = () => {
  return (
    <div className="about">
      <Container>
        <Row>
          <div className="top h1 d-flex justify-content-center">About Me</div>
          <div className="bottom">
            <div className="col-4">
              {" "}
              <div className="img w-100">
                <img className="about-img" src={img} />
                <span className="text-black-50 d-flex justify-content-center">
                  S.Web Developer
                </span>
              </div>
            </div>
            <div className="col-8 h-auto">
              {" "}
              <div className="content text-dark-50  w-100">
                <div className="name h3">D.John</div>
                <p className="text-black-50 lh-lg">
                  ipsum dolor sit amet, consectetur adipisicing elit, sed do
                  eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                  enim ad minim veniam, quis nostrud exercitation ullamco
                  laboris nisi ut aliquip ex ea commodo consequat. Duis aute
                  irure dolor in reprehenderit in voluptate velit esse cillum
                  dolore eu fugiat nulla pariatur. Excepteur sint occaecat
                  cupidatat non proident, sunt in culpa qui officia deserunt
                  mollit anim id est laborum.
                </p>
              </div>
            </div>
          </div>
        </Row>
      </Container>
    </div>
  );
};

export default About;
