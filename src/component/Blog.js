import React from "react";
import { Container, Row } from "react-bootstrap";
const Blog = () => {
  return (
    <div className="portfolio">
      <Container className=" p-5">
        <Row>
          <div
            className="top h1 d-flex justify-content-center py-2 pb-5"
            style={{ color: "#F97300" }}
          >
            Blog
          </div>
          <div className="bottom pt-5">
            <div className="col-12 d-flex">
              <div className="col-4 p-3">
                <div className="card w-100" style={{ width: "18rem" }}>
                  <img
                    src={require("../accset/1005-5760x3840.jpg")}
                    className="card-img-top"
                  />
                  <div className="card-body">
                    <h4 className="card-title">Post Title</h4>
                    <p className="card-text">
                      proident, sunt in culpa qui officia deserunt mollit anim
                      id est laborum.
                    </p>
                  </div>

                  <div
                    className="card-body"
                    style={{ borderTop: "1px solid rgba(0, 0, 0, 0.125)" }}
                  >
                    <a
                      href="#"
                      className="card-link"
                      style={{ textDecoration: "none" }}
                    >
                      Read More
                    </a>
                  </div>
                </div>
              </div>
              <div className="col-4 p-3">
                <div className="card w-100" style={{ width: "18rem" }}>
                  <img
                    height="250"
                    src={require("../accset/1009-5000x7502.jpg")}
                    className="card-img-top "
                  />
                  <div className="card-body">
                    <h4 className="card-title">Post Title</h4>
                    <p className="card-text">
                      proident, sunt in culpa qui officia deserunt mollit anim
                      id est laborum.
                    </p>
                  </div>

                  <div
                    className="card-body"
                    style={{ borderTop: "1px solid rgba(0, 0, 0, 0.125)" }}
                  >
                    <a
                      href="#"
                      className="card-link"
                      style={{ textDecoration: "none" }}
                    >
                      Read More{" "}
                    </a>
                  </div>
                </div>
              </div>
              <div className="col-4 p-3">
                <div className="card w-100" style={{ width: "18rem" }}>
                  <img
                    src={require("../accset/1010-5184x3456.jpg")}
                    className="card-img-top"
                  />
                  <div className="card-body">
                    <h4 className="card-title">Post Title</h4>
                    <p className="card-text">
                      proident, sunt in culpa qui officia deserunt mollit anim
                      id est laborum.
                    </p>
                  </div>

                  <div
                    className="card-body"
                    style={{ borderTop: "1px solid rgba(0, 0, 0, 0.125)" }}
                  >
                    <a
                      href="#"
                      className="card-link"
                      style={{ textDecoration: "none" }}
                    >
                      Read More{" "}
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </Row>
      </Container>
    </div>
  );
};

export default Blog;
