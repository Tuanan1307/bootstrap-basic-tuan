import "./App.css";
import About from "./component/About";
import Blog from "./component/Blog";
import Footer from "./component/Footer";
import GetinTouch from "./component/GetinTouch";
import Header from "./component/Header";

import Menu from "./component/Menu";
import OurTeam from "./component/OurTeam";
import Portfolio from "./component/Portfolio";
function App() {
  return (
    <div className="App">
      <Menu />
      <Header />
      <About />
      <Portfolio />
      <Blog />
      <OurTeam />
      <GetinTouch />
      <Footer />
    </div>
  );
}

export default App;
